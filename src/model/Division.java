package model;

import individuals.Doctor;
import individuals.Nurse;
import individuals.Patient;

import java.util.ArrayList;

public class Division {
	private int id;
	private ArrayList<Nurse> nurses;
	private ArrayList<Doctor> doctors;
	private ArrayList<Patient> patients;

	public Division(int id){
		this.id = id;
		nurses = new ArrayList<Nurse>();
		doctors = new ArrayList<Doctor>();
		patients = new ArrayList<Patient>();
	}
	
	public void addNurse(Nurse nurse){
		nurses.add(nurse);
	}
	
	public void addDoctor(Doctor doctor){
		doctors.add(doctor);
	}
	
	public void addPatient(Patient patient){
		patients.add(patient);
	}
	
	public ArrayList<Nurse> getNurses(){
		return nurses;
	}

	public ArrayList<Doctor> getDoctors(){
		return doctors;
	}

	public ArrayList<Patient> getPatients(){
		return patients;
	}

	public Patient getPatient(String name){
		for(int i = 0; i < patients.size(); i++){
			if(patients.get(i).getName().equals(name)){
				return patients.get(i);
			}
		}
		return null; 
	}
}
