package model;

import individuals.Doctor;
import individuals.Nurse;
import individuals.Patient;

import java.util.ArrayList;

public class Journal {
	private Patient patient;
	private Doctor doctor;
	private Nurse nurse;
	private ArrayList<Division> divisionList;
	private StringBuilder sb;
	private Doctor readDoctor;
	private Nurse readNurse;
	private String comment;


	public Journal(Patient patient, Doctor doctor, Nurse nurse) {
		this.patient = patient;
		this.doctor = doctor;
		this.nurse = nurse;
		comment = "Medical comment: ";
	}

	public void editComment(String text){
		comment = "Medical comment: " + text; 
	}
	
	public String getComment(){
		return comment;
	}
	
	public boolean nurseAllowedToRead(Nurse nurse) {
		if (this.nurse.getName() == nurse.getName()) {
			return true;
		} else {
			for (int i = 0; i < divisionList.size(); i++) {
				for (int j = 0; j < (divisionList.get(i).getNurses().size()); j++) {
					readNurse = divisionList.get(i).getNurses().get(j);
					if (readNurse.getName() == doctor.getName()) {
						return true;
					}
				}
			}
			return false;
		}

	}

	public String getJournal() {
		sb = new StringBuilder();
		sb.append(patient.toString()+"\n");
		sb.append("Doctor: " + doctor.getName()+"\n");
		sb.append("Nurse: " + nurse.getName()+"\n");
		sb.append(comment+"\n");
		return sb.toString();
	}

}
