package model;

import individuals.Doctor;
import individuals.Government;
import individuals.Individual;
import individuals.Nurse;
import individuals.Patient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

public class FileManager {
	private Writer output;
	private ArrayList<Patient> patients;
	private ArrayList<Individual> i;

	public FileManager() throws IOException {
		File file = new File("database.txt");
		patients = new ArrayList<Patient>();
		output = new BufferedWriter(new FileWriter(file, true));
		BufferedReader br = new BufferedReader(new FileReader(file));
		StringBuilder sb = new StringBuilder();
		i= new ArrayList<Individual>();
		
		Government government = new Government("SWE", 12);
		Division division = new Division(1);
		Doctor doctor = new Doctor("Peter", division, 111);
		Nurse nurse = new Nurse("Joel", division, 222);
		Patient patient1 = new Patient("Axel", division, 123);
		Patient patient2 = new Patient("Felicia", division, 456);
		Patient patient3 = new Patient("Paula", division, 789);
		i.add(patient1);
		i.add(patient2);
		i.add(patient3);
		i.add(nurse);
		i.add(doctor);
		i.add(government);
		
		
		patients.add(patient1);
		patients.add(patient2);
		patients.add(patient3);
		String line = br.readLine();
		String[] s ;
		while (line != null) {
			s = line.split(" ");
			Patient p = getPatient(Integer.parseInt(s[1]));
			Journal j = new Journal( p, doctor, nurse);
			p.addJournal(j);
			if(s.length>4){
				j.editComment(s[4]);
			}
			doctor.addPatients(p);
			nurse.addPatients(p);
			line = br.readLine();
			
		}

	}

	private Patient getPatient(int id) {
		for (int i = 0; i < patients.size(); i++) {
			Patient p = patients.get(i);
			if (p.getId() == id) {
				return p;
			}
		}
		return null;
	}

	public void save(String options) throws IOException {
		output.append(options);
		output.close();
	}

	public ArrayList<Individual> getUser() {

		return i;

	}
}
