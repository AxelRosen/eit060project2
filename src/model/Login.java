package model;

import individuals.Doctor;
import individuals.Government;
import individuals.Individual;
import individuals.Nurse;
import individuals.Patient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class Login {
	private HashMap<byte[], Individual> users = new HashMap<byte[], Individual>();
	private int id;
	private String password;
	private Individual loggedIn;
	private Individual i;
	Set<byte[]> keys;
	Iterator<byte[]> keyIterator;
	private ArrayList<Patient> allPatients;
	private ArrayList<Nurse> allNurses;

	Government government = new Government("SWE", 12);
	Division division = new Division(1);
	Doctor doctor = new Doctor("Peter", division, 111);
	Nurse nurse = new Nurse("Joel", division, 222);
	Patient patient1 = new Patient("Axel", division, 123);
	Patient patient2 = new Patient("Felicia", division, 456);
	Patient patient3 = new Patient("Paula", division, 789);

	Journal journal1 = new Journal(patient1, doctor, nurse);
	Journal journal2 = new Journal(patient2, doctor, nurse);

	private FileManager fm;

	PublicKey publicKey = readPublicKey("public.der");
	PrivateKey privateKey = readPrivateKey("private.der");

	public Login() throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeySpecException, IOException {
		allPatients = new ArrayList<Patient>();
		fm = new FileManager();
		ArrayList<Individual> i = fm.getUser();
		users.put(encryptPassword("hej1"), i.get(0));
		users.put(encryptPassword("hej2"), i.get(1));
		users.put(encryptPassword("hej3"), i.get(2));
		users.put(encryptPassword("hej4"), i.get(3));
		users.put(encryptPassword("hej5"), i.get(4));
		users.put(encryptPassword("hej6"), i.get(5));
	}

	private boolean correctUser() {

		keys = users.keySet();
		keyIterator = keys.iterator();
		while (keyIterator.hasNext()) {
			byte[] nextKey = keyIterator.next();
			byte[] decryptedServer = decryptPassword(nextKey);
			byte[] encryptedClient = encryptPassword(password);
			if ((new String(decryptedServer)).equals(password)
					&& (users.get(nextKey).getId() == id)) {
				System.out.println("User found in the system, logging in.");
				loggedIn = users.get(nextKey);
				return true;
			}
		}
		return false;
	}


	public Individual getUser(int id, String password) {
		this.id = id;
		this.password = password;
		if (correctUser()) {
			return loggedIn;
		}
		return null;

	}

	public boolean individualExists(int splitString) {
		Collection<Individual> individuals = users.values();
		Iterator<Individual> i = individuals.iterator();
		while (i.hasNext()) {
			if (i.next().getId() == splitString) {
				return true;
			}
		}
		return false;

	}

	public ArrayList<Patient> getAllPatients() {
		System.out.println("getting all patients in login");
		Collection<Individual> individuals = users.values();
		Iterator<Individual> i = individuals.iterator();
		while (i.hasNext()) {
			Individual individual = i.next();
			if (individual.getSecurity() == 1) {
				allPatients.add((Patient) individual);
			}
		}
		return allPatients;
	}

	public ArrayList<Nurse> getAllNurses() {
		System.out.println("getting all nurses in login");
		Collection<Individual> individuals = users.values();
		Iterator<Individual> i = individuals.iterator();
		while (i.hasNext()) {
			Individual individual = i.next();
			if (individual.getSecurity() == 2) {
				allNurses.add((Nurse) individual);
			}
		}
		return allNurses;
	}

	public byte[] encrypt(PublicKey key, byte[] plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(plaintext);
	}

	public byte[] decrypt(PrivateKey key, byte[] ciphertext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(ciphertext);
	}

	public static PublicKey readPublicKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(publicSpec);
	}

	public static PrivateKey readPrivateKey(String filename)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

	public static byte[] readFileBytes(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	public byte[] encryptPassword(String password) {
		try {
			return encrypt(publicKey, (password).getBytes("UTF8"));
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return null;
	}

	private byte[] decryptPassword(byte[] key) {
		try {
			return decrypt(privateKey, key);
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
