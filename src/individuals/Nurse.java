package individuals;

import java.util.ArrayList;

import model.Division;
import model.Journal;

public class Nurse extends Individual{
	private String name;
	private int security = 2;
	private Division division;
	private ArrayList<Patient> patients;
	private ArrayList<Patient> divisionPatients;

	private StringBuilder sb = new StringBuilder();
	private int id;

	public Nurse(String name, Division division, int id) {
		this.name = name;
		this.division = division;
		patients = new ArrayList<Patient>();
		this.id = id;
	}
	
	public String getName(){
		return name;
	}

	public void setSecurityLevel(int security) {
		this.security = security;
	}

	public int getSecurity(){
		return security;
	}
	
	public String toString() {
		return "Name: " + name + "Security level: "
				+ security;
	}
	
	public String readJournal(Journal journal){
		if(journal.nurseAllowedToRead(this)){
			return journal.getJournal();
		}
		return "Not allowed to read this journal";
	}
	
	public void addPatients(Patient patient){
		patients.add(patient);
	}
	
	public String listAllPatients() {
		sb = new StringBuilder();
		for(int i = 0; i < patients.size(); i++){
			sb.append(patients.get(i).getName()+"\n");
		}
		return sb.toString();
	}
	
	public String listDivisionPatients(){
		sb = new StringBuilder();
		for(int i = 0; i< division.getPatients().size(); i++){
			sb.append(division.getPatients().get(i).getName()+"\n");
		}
		return sb.toString();
	}

	public Division getDivision() {
		return division;
	}

	public boolean searchForDivisionPatient(String patient) {
		for(int i = 0; i < division.getPatients().size(); i++){
			if(patient.equals(division.getPatients().get(i).getName())){
				return true;
			}
		}
		return false;
	}
	public boolean searchForPatient(String patient) {
	for(int i = 0; i < patients.size(); i++){
		if(patient.equals(patients.get(i).getName()) || Integer.parseInt(patient) == (patients.get(i).getId())){
			return true;	
		}
	}
	return false;
}
	
	public int getId(){
		return id;

	}

}
