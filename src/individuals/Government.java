package individuals;

import java.util.ArrayList;

import model.Division;
import model.Login;

public class Government extends Individual {
	private String name;
	private int security = 4;
	private int id;
	private Login login;
	private ArrayList<Patient> allPatients;
	
	public Government(String name, int id) {
		this.name = name;
		this.id = id;
		allPatients = new ArrayList<Patient>();
	}

	public void setSecurityLevel(int security) {
		this.security = security;
	}

	public int getSecurity() {
		return security;
	}

	public String toString() {
		return null;
	}

	public int getId() {
		return id;
	}

	public ArrayList<Patient> listAllPatients() {
		return allPatients;
	}
	
	public void setLogin(Login login){
		this.login = login;
	}
	
	public Patient getPatient(String name){
		for(int i = 0; i < allPatients.size(); i++){
			Patient p = allPatients.get(i);
			if(p.getName().equals(name)){
				return p;
			}
		}
		return null;
	}
	
	
	public boolean searchForDivisionPatient(String patient) {
		allPatients = login.getAllPatients();
		for(int i = 0; i< allPatients.size(); i ++){
			if(allPatients.get(i).getName().equals(patient) ){
				return true;
			}
		}
		return false;
	}
}
