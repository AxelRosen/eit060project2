package individuals;

import java.util.ArrayList;

import model.Division;
import model.Journal;


public class Doctor extends Individual{
	private String name;
	private int security = 3;
	private Division division;
	private ArrayList<Division> divisionList;
	private ArrayList<Patient> patients;
	private ArrayList<Patient> divisionPatients;
	private StringBuilder sb = new StringBuilder();
	private int id;

	public Doctor(String name, Division division, int id) {
		this.name = name;
		this.division = division;
		patients = new ArrayList<Patient>();
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setSecurityLevel(int security) {
		this.security = security;
	}

	public int getSecurity() {
		return security;
	}

	public void createJournal(String info) {
			String[] splitString = info.split(" ");
			Patient p = getPatient(Integer.parseInt(splitString[0]));
			Nurse nurse = new Nurse(splitString[1].toString(), division, 123);
			p.addJournal(new Journal(p, this, nurse));
			System.out.println("Journal created");
	}


	public String toString() {
		return null;
	}
	public int getId(){
		return id;
	}

	public void addPatients(Patient patient){
		patients.add(patient);
	}
	
	public Patient getPatient(int id){
		for(int i = 0; i < patients.size(); i ++){
			Patient p = patients.get(i);
			if(p.getId() == id){
				return p;
			}
		}
		return null;
	}
	
	public String listAllPatients() {
		sb = new StringBuilder();
		for(int i = 0; i < patients.size(); i++){
			sb.append(patients.get(i).getName()+"\n");
		}
		return sb.toString();
	}
	
	public String listDivisionPatients(){
		sb = new StringBuilder();
		for(int i = 0; i< division.getPatients().size(); i++){
			sb.append(division.getPatients().get(i).getName()+"\n");
		}
		return sb.toString();
	}

	public Division getDivision() {
		return division;
	}

	public boolean searchForDivisionPatient(String patient) {
		for(int i = 0; i < division.getPatients().size(); i++){
			if(patient.equals(division.getPatients().get(i).getName())){
				return true;
			}
		}
		return false;
	}
	
	public boolean searchForPatient(String patient) {
		for(int i = 0; i < patients.size(); i++){
			if(patient.equals(patients.get(i).getName()) || (Integer.parseInt(patient) == (patients.get(i).getId()))){
				return true;	
			}
		}
		return false;
	}

}
