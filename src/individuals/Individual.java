package individuals;

public abstract class Individual {

	public abstract void setSecurityLevel(int security);

	public abstract int getId();

	public abstract String toString();

	public abstract int getSecurity();

	public boolean searchForDivisionPatient(String option) {
		return false;
	}

	public boolean searchForPatient(String option) {
		return false;
	}



}
