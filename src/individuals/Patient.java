package individuals;

import java.util.ArrayList;

import model.Division;
import model.Journal;


public class Patient extends Individual {
	private String name;
	private int security = 1;
	private ArrayList<Journal> journals;
	private StringBuilder sb;
	private int id;
	

	public Patient(String name,  Division division, int id) {
		this.name = name;
		this.id=id;
		division.addPatient(this);
		journals = new ArrayList<Journal>();
		sb = new StringBuilder();
	}

	public String getName(){
		return name;
	}
	
	public void setSecurityLevel(int security) {
		this.security = security;
	}

	public int getSecurity() {
		return security;
	}
	
	public void addJournal(Journal journal){
		journals.add(journal);
	}
	public String getJournals() {
		if(!journals.isEmpty()){
		for (int i = 0; i < journals.size(); i++) {
			sb.append(journals.get(i).getJournal()+"\n");
		}
		return sb.toString();
		}
		return "No journals found";
	}

	public String toString() {
		return "Name: " + name  + " ID number: " + id +"\n";
	}
	
	public Journal getJournal(){
		return journals.get(0);
	}

	public int getId(){
		return id;
	}

	public void deleteJournal() {
		System.out.println("Deleted the journals");
		journals.clear();
		journals = new ArrayList<Journal>();
	}

}
