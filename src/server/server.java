package server;

import individuals.Doctor;
import individuals.Nurse;
import individuals.Patient;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.*;
import javax.net.ssl.*;
import javax.security.cert.X509Certificate;

import model.Division;
import model.Journal;

public class server implements Runnable {
	private ServerSocket serverSocket = null;
	private StringBuilder sb;
	private StringBuilder log;

	public server(ServerSocket ss) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		serverSocket = ss;
		newListener();
		sb = new StringBuilder();
		log = new StringBuilder();
	}

	public void run() {
		ServerCommands commands;
		try {
			commands = new ServerCommands();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeySpecException | IOException e1) {
			e1.printStackTrace();
		}
		byte[] flag = new byte[1];
		byte[] buffer = new byte[257];
		try {
			SSLSocket socket = (SSLSocket) serverSocket.accept();
			newListener();
			SSLSession session = socket.getSession();
			X509Certificate cert = (X509Certificate) session
					.getPeerCertificateChain()[0];
			String subject = cert.getSubjectDN().getName();
			String issuer = cert.getIssuerDN().getName();
			PrintWriter out = null;
			BufferedReader in = null;
			DataInputStream dIn = null;
			DataOutputStream dOut = null;
			dOut = new DataOutputStream(socket.getOutputStream());
			dIn = new DataInputStream(socket.getInputStream());
			byte[] clientMessage = null;
			String rec;
			String recDecrypted = null;
			byte[] recoveredMessage;
			byte[] encryptedMsg;
			PublicKey publicKey = readPublicKey("public.der");
			PrivateKey privateKey = readPrivateKey("private.der");
			commands = new ServerCommands();
			sb.setLength(0);

			String command = "Wrong username or password, please try again";
			while (command
					.equals("Wrong username or password, please try again")) {
				int lengthOfLogin = dIn.readInt();
				if (lengthOfLogin > 0) {
					clientMessage = new byte[lengthOfLogin];
					dIn.readFully(clientMessage, 0, clientMessage.length);
				}
				recoveredMessage = decrypt(privateKey, clientMessage);
				String info = new String(recoveredMessage,
						StandardCharsets.UTF_8);
				command = commands.login(info);
				encryptedMsg = encrypt(publicKey, commands.login(info)
						.getBytes("UTF8"));
				dOut.writeInt(encryptedMsg.length);
				dOut.write(encryptedMsg);
				dOut.flush();
			}
			while (true) {
				sb.setLength(0);
				int length = dIn.readInt();
				if (length > 0) {
					clientMessage = new byte[length];
					dIn.readFully(clientMessage, 0, clientMessage.length);
				}
				recoveredMessage = decrypt(privateKey, clientMessage);
				String recoveredString = new String(recoveredMessage,
						StandardCharsets.UTF_8);

				System.out.println("Fetching data from database\n");
				System.out
						.println("New recovered string: \n" + recoveredString);
				sb.append(commands.handleMessage(recoveredString));
				encryptedMsg = encrypt(publicKey, sb.toString()
						.getBytes("UTF8"));

				dOut.writeInt(encryptedMsg.length);
				dOut.write(encryptedMsg);
				dOut.flush();

				System.out.println("received '\n" + recoveredString
						+ "' from client");
				System.out.print("sending \n'" + sb.toString()
						+ "'\nto client...\n");
				System.out.println("done\n");
			}
		} catch (IOException e) {
			System.out.println("Client turned off");
			return;
		} catch (Exception e) {

		}
	}

	private void newListener() {
		(new Thread(this)).start();
	} 

	public static byte[] encrypt(PublicKey key, byte[] plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(plaintext);
	}

	public byte[] decrypt(PrivateKey key, byte[] ciphertext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(ciphertext);
	}

	public static byte[] readFileBytes(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	public static PublicKey readPublicKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(publicSpec);
	}

	public static PrivateKey readPrivateKey(String filename)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

	public static void main(String args[]) throws NoSuchAlgorithmException,
			InvalidKeySpecException {
		System.out.println("\nServer Started\n");
		int port = -1;
		if (args.length >= 1) {
			port = Integer.parseInt(args[0]);
		}
		String type = "TLS";
		try {
			ServerSocketFactory ssf = getServerSocketFactory(type);
			ServerSocket ss = ssf.createServerSocket(port);
			((SSLServerSocket) ss).setNeedClientAuth(true); // enables client
															// authentication
			new server(ss);
		} catch (IOException e) {
			System.out.println("Unable to start Server: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static ServerSocketFactory getServerSocketFactory(String type) {
		if (type.equals("TLS")) {
			SSLServerSocketFactory ssf = null;
			try { 
				SSLContext ctx = SSLContext.getInstance("TLS");
				KeyManagerFactory kmf = KeyManagerFactory
						.getInstance("SunX509");
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance("SunX509");
				KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
				char[] password = "password".toCharArray();

				ks.load(new FileInputStream("serverkeystore"), password); 
				ts.load(new FileInputStream("servertruststore"), password);

				kmf.init(ks, password); 
				tmf.init(ts); 
				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
				ssf = ctx.getServerSocketFactory();
				return ssf;
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			return ServerSocketFactory.getDefault();
		}
		return null;
	}
}
