package server;

import individuals.Doctor;
import individuals.Government;
import individuals.Nurse;
import individuals.Patient;
import individuals.Individual;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import model.Division;
import model.Journal;
import model.Login;

public class ServerCommands {
	private static final String READJOURNAL = "1";
	private static final String WRITEJOURNAL = "2";
	private static final String CREATEJOURNAL = "3";
	StringBuilder sb;
	LinkedList<String> lastOptions;
	private String patientName;
	private StringBuilder log;
	FileWriter fw;
	private Login login;
	private Individual loggedIn;
	private String type;

	public ServerCommands() throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, IOException {
		sb = new StringBuilder();
		log = new StringBuilder();
		lastOptions = new LinkedList<String>();
		String patientName = "";
		try {
			Runtime.getRuntime().exec("chmod 700 log.txt");
			Runtime.getRuntime().exec("chmod 700 src/log.txt");

			fw = new FileWriter("log.txt", true);

		} catch (IOException e) {
			e.printStackTrace();
		}
		login = new Login();
	}

	public String login(String info) {
		String[] s = info.split(" ");
		try {
			login = new Login();
		} catch (InvalidKeyException | NoSuchAlgorithmException
				| NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | InvalidKeySpecException | IOException e) {
			e.printStackTrace();
		}
		int i = 0;
		try {
			i = Integer.parseInt(s[0]);

		} catch (NumberFormatException n) {

		}
		System.out.println("fetching the user");
		loggedIn = login.getUser(i, s[1]);
		if ((loggedIn != null) && login.individualExists(loggedIn.getId())) {
			switch (loggedIn.getSecurity()) {
			case (1):
				return "These are your options: \n1: Read Journal\nEnter your choice:";
			case (2):
				return "These are your options: \n1: List patients with read access\n2: List patients with read and write access \nEnter your choice:";
			case (3):
				return "These are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create Journal \nEnter your choice:";
			case (4):
				return "These are your options: \n1: List patients with read access\n2: Delete a journal \nEnter your choice:";
			}
		}
		return "Wrong username or password, please try again";

	}

	public String handleMessage(String option) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException,
			InvalidKeySpecException, IOException {
		sb.setLength(0);

		if (loggedIn.getSecurity() == 4) {
			((Government) loggedIn).setLogin(login);
		}

		if (loggedIn.searchForDivisionPatient(option)) {
			patientName = option;
			option = "USERFOUND";
		}

		if (loggedIn.getSecurity() == 2) {
			if (!lastOptions.isEmpty() && lastOptions.peek().equals("yes")) {
				lastOptions.poll();
				if (lastOptions.peek().equals("2")) {
					((Nurse) loggedIn).getDivision().getPatient(patientName)
							.getJournal().editComment(option);
					sb.append("Edited the comment of the patient to:\n");
					sb.append(((Nurse) loggedIn).getDivision()
							.getPatient(patientName).getJournal().getComment()
							+ "\n\n");
					writeToLog("Edited comment of patient with ID number: "
							+ ((Nurse) loggedIn).getDivision()
									.getPatient(patientName).getId());
					lastOptions.poll();
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \nEnter your choice:");
					return sb.toString();
				}
			}
		}

		if (loggedIn.getSecurity() == 3) {
			if (!lastOptions.isEmpty()
					&& lastOptions.peek().equals(CREATEJOURNAL)) {
				String[] splitString = option.split(" ");
				if (((Doctor) loggedIn).searchForPatient(splitString[0])
						&& login.individualExists(Integer
								.parseInt(splitString[1]))) {
					((Doctor) loggedIn).createJournal(option);
					sb.append("Successfully created journal\n\nThese are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create new journal \nEnter your choice:");
					System.out.println("appended the text" + option);
					writeToLog("Read Journal of patient with ID number: "
							+ splitString[0]);
					lastOptions.poll();

				} else {
					sb.append("Wrong patient or nurse ID, please try again");
				}
				return sb.toString();
			}
			if (!lastOptions.isEmpty() && lastOptions.peek().equals("yes")) {
				lastOptions.poll();
				if (lastOptions.peek().equals("2")) {
					System.out
							.println("THIS IS THE PATIENTNAME " + patientName);
					((Doctor) loggedIn).getDivision().getPatient(patientName)
							.getJournal().editComment(option);
					sb.append("Edited the comment of the patient to:\n");
					sb.append(((Doctor) loggedIn).getDivision()
							.getPatient(patientName).getJournal().getComment()
							+ "\n\n");
					writeToLog("Edited comment of patient with ID number: "
							+ ((Doctor) loggedIn).getDivision()
									.getPatient(patientName).getId());
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create new journal \nEnter your choice:");
					lastOptions.poll();
					return sb.toString();
				}
			}
		}

		switch (loggedIn.getSecurity()) {
		case (1):
			switch (option) {
			case (READJOURNAL):
				writeToLog("Read their own journal");
				return ((Patient) loggedIn).getJournals();
			}
		case (2):
			switch (option) {
			case (READJOURNAL):
				sb.append("Select what patient:\n");
				sb.append("Read access:\n");
				sb.append(((Nurse) loggedIn).listDivisionPatients());
				lastOptions.push(option);
				return sb.toString();
			case ("USERFOUND"):
				if (lastOptions.peek().equals("1")) {
					sb.append(((Nurse) loggedIn).getDivision()
							.getPatient(patientName).getJournals()
							+ "\n\n");
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create new journal \nEnter your choice:");
					writeToLog("Read Journal of patient with ID number: "
							+ ((Nurse) loggedIn).getDivision()
									.getPatient(patientName).getId());
					lastOptions.poll();
					return sb.toString();
				}
				if (lastOptions.peek().equals("2")) {
					sb.append("This is the current journal:\n"
							+ ((Nurse) loggedIn).getDivision()
									.getPatient(patientName).getJournals());
					writeToLog("Read Journal of patient with ID number: "
							+ ((Nurse) loggedIn).getDivision()
									.getPatient(patientName).getId());
					sb.append("Would you like to edit the comment of this patient? yes/no ");
					lastOptions.push(option);

					return sb.toString();
				}
			case (WRITEJOURNAL):
				sb.append("You have read and write access to these patients:\n");
				sb.append(((Nurse) loggedIn).listAllPatients() + "\n");
				sb.append("Select what patient you want to access:");
				return sb.toString();

			case ("yes"):
				if (!lastOptions.isEmpty() && lastOptions.peek().equals("2")) {
					sb.append("Enter the new comment:\n");
					lastOptions.poll();
					return sb.toString();
				}
			case ("no"):
				if (!lastOptions.isEmpty() && lastOptions.peek().equals("2")) {
					sb.append("Back to root menu: ");
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \nEnter your choice:");
					return sb.toString();
				}
			}

		case (3):
			switch (option) {
			case (READJOURNAL):
				sb.append("Select what patient:\n");
				sb.append("Read access:\n");
				sb.append(((Doctor) loggedIn).listDivisionPatients());
				lastOptions.push(option);
				return sb.toString();

			case (WRITEJOURNAL):
				sb.append("You have read and write access to these patients:\n");
				sb.append(((Doctor) loggedIn).listAllPatients() + "\n");
				sb.append("Select what patient you want to access:");
				lastOptions.push(option);
				return sb.toString();

			case ("USERFOUND"):
				if (lastOptions.peek().equals("1")) {
					sb.append(((Doctor) loggedIn).getDivision()
							.getPatient(patientName).getJournals()
							+ "\n\n");
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create new journal \nEnter your choice:");
					writeToLog("Read Journal of patient with ID number: "
							+ ((Doctor) loggedIn).getDivision()
									.getPatient(patientName).getId());
					lastOptions.poll();
					return sb.toString();
				}
				if (lastOptions.peek().equals("2")) {
					sb.append("The current state of the journal:\n"
							+ ((Doctor) loggedIn).getDivision()
									.getPatient(patientName).getJournals());
					writeToLog("Read Journal of patient with ID number: "
							+ ((Doctor) loggedIn).getDivision()
									.getPatient(patientName).getId());
					sb.append("Would you like to edit the comment of this patient? yes/no ");
					return sb.toString();
				}

			case ("yes"):
				if (!lastOptions.isEmpty() && lastOptions.peek().equals("2")) {
					sb.append("Enter the new comment:\n");
					lastOptions.push(option);
					return sb.toString();
				}
			case ("no"):
				if (!lastOptions.isEmpty() && lastOptions.peek().equals("2")) {
					sb.append("Back to root menu: ");
					sb.append("These are your options: \n1: List patients with read access\n2: List patients with read and write access \n3: Create Journal \nEnter your choice:");
					return sb.toString();
				}

			case (CREATEJOURNAL):
				if (loggedIn.getSecurity() == 3 && lastOptions.isEmpty()) {
					lastOptions.push(option);
					sb.append("Please enter the journal info exactly as the following:\n");
					sb.append("PatientID NurseID");
					return sb.toString();
				}
			}

		case (4):
			sb.setLength(0);
			switch (option) {
			case (READJOURNAL):
				sb.append("Select what patient:\n");
				sb.append("Read access:\n");
				sb.append(((Government) loggedIn).listAllPatients());
				lastOptions.push(option);
				if (sb.length() > 255) {
					String s = sb.substring(0, 120);
					return s;
				}
				return sb.toString();

			case ("2"):
				sb.append("Select what patient to delete the journal of:\n");
				sb.append(((Government) loggedIn).listAllPatients());
				lastOptions.push(option);
				return sb.toString();

			case ("USERFOUND"):
				if (!lastOptions.isEmpty()) {
					if (lastOptions.peek().equals("1")) {
						sb.append(((Government) loggedIn).getPatient(
								patientName).getJournals()
								+ "\n\n");
						sb.append("These are your options: \n1: List patients with read access\n2: Delete a journal \nEnter your choice:");
						writeToLog("Read Journal of patient with ID number: "
								+ ((Government) loggedIn).getPatient(
										patientName).getId());
						lastOptions.poll();
						return sb.toString();
					}
					if (lastOptions.peek().equals("2")) {
						sb.append("Deleted the journal of the patient "
								+ patientName + "\n\n");
						((Government) loggedIn).getPatient(patientName)
								.deleteJournal();
						sb.append("These are your options: \n1: List patients with read access\n2: Delete a journal \nEnter your choice:");
						writeToLog("Deleted the journal with ID number: "
								+ ((Government) loggedIn).getPatient(
										patientName).getId());
						lastOptions.poll();
						return sb.toString();
					}
				}
			}
		}

		return "No such option, please choose again\n" + sb.toString();
	}

	private void writeToLog(String text) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		log.append(timestamp + "\t");
		log.append(loggedIn.getId() + "\t");
		log.append(text + "\n");
		writeLogToFile();
	}

	public void writeLogToFile() {
		try {
			fw.write(log.toString() + "\n");
			fw.flush();
			Runtime.getRuntime().exec("chmod 400 log.txt");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
