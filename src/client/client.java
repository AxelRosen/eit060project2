package client;

import individuals.Doctor;
import individuals.Nurse;
import individuals.Patient;

import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.*;
import javax.security.cert.X509Certificate;

import model.Division;
import model.Journal;

import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

public class client {
	private final String PATIENTREAD = "Flag:PatientRead";
	private static ClientCommands commands = new ClientCommands();


	public static void main(String[] args) throws Exception {
		Console console = System.console();
		PublicKey publicKey = readPublicKey("public.der");
		PrivateKey privateKey = readPrivateKey("private.der");

		byte[] encryptedMsg;


		StringBuilder sb = new StringBuilder();

		String host = null;
		int port = -1;
		for (int i = 0; i < args.length; i++) {
		}
		if (args.length < 2) {
			System.exit(-1);
		}
		try { 
			host = args[0];
			port = Integer.parseInt(args[1]);
		} catch (IllegalArgumentException e) {
			System.exit(-1);
		}

		try { 
			SSLSocketFactory factory = null;
			try {
				char[] password = "password".toCharArray();
				KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
				KeyManagerFactory kmf = KeyManagerFactory
						.getInstance("SunX509");
				TrustManagerFactory tmf = TrustManagerFactory
						.getInstance("SunX509");
				SSLContext ctx = SSLContext.getInstance("TLS");
				
				ks.load(new FileInputStream("clientkeystore"), password); 
				ts.load(new FileInputStream("clienttruststore"), password); 
				kmf.init(ks, password); 
				tmf.init(ts);
				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
				factory = ctx.getSocketFactory();
			} catch (Exception e) {
				throw new IOException(e.getMessage());
			}
			SSLSocket socket = (SSLSocket) factory.createSocket(host, port);

			socket.startHandshake();

			SSLSession session = socket.getSession();
			X509Certificate cert = (X509Certificate) session
					.getPeerCertificateChain()[0];
			String subject = cert.getSubjectDN().getName();
			String issuer = cert.getIssuerDN().getName();
			DataOutputStream dOut = new DataOutputStream(
					socket.getOutputStream());
			DataInputStream dIn = new DataInputStream(socket.getInputStream());

			BufferedReader read = new BufferedReader(new InputStreamReader(
					System.in));
			String username = "";
			char[] password;

			String recoveredOptions = "Wrong username or password, please try again";
			while (recoveredOptions
					.equals("Wrong username or password, please try again")) {
				StringBuilder login = new StringBuilder();
				
				System.out.println("Username: ");
				username = console.readLine();
				login.append(username + " ");
				System.out.println("Password: ");
				password = console.readPassword();
				login.append(String.valueOf(password));

				encryptedMsg = encrypt(publicKey,
						login.toString().getBytes("UTF8"));
				dOut.writeInt(encryptedMsg.length);
				dOut.write(encryptedMsg);
				dOut.flush();

				byte[] recievedOptions = null;
				String[] flag1;
				int lengthOfOptions = dIn.readInt();
				if (lengthOfOptions > 0) {
					recievedOptions = new byte[lengthOfOptions];
					dIn.readFully(recievedOptions, 0, recievedOptions.length);
				}
				recievedOptions = decrypt(privateKey, recievedOptions);
				recoveredOptions = new String(recievedOptions,
						StandardCharsets.UTF_8);
				System.out.println(recoveredOptions + "\n");
			}

			while (true) {

				String msg;
				System.out.print(">");
				msg = read.readLine();
				encryptedMsg = encrypt(publicKey, msg.getBytes("UTF8"));
				dOut.writeInt(encryptedMsg.length); 
				dOut.write(encryptedMsg); 
				dOut.flush();

				System.out.println("received the following from the server:\n");

				byte[] recievedMessage = null;
				String[] flag;

				int length = dIn.readInt();
				if (length > 0) {
					recievedMessage = new byte[length];
					dIn.readFully(recievedMessage, 0, recievedMessage.length);
				}
				recievedMessage = decrypt(privateKey, recievedMessage);
				String recoveredString = new String(recievedMessage,
						StandardCharsets.UTF_8);

				if (recoveredString.equals("quit")) {
					System.out.println("Exiting system");
					break;
				}
				System.out.println(recoveredString + "\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static byte[] encrypt(PublicKey key, byte[] plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(plaintext);
	}

	//
	public static byte[] decrypt(PrivateKey key, byte[] ciphertext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(ciphertext);
	}

	public static byte[] readFileBytes(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	public static PublicKey readPublicKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(publicSpec);
	}

	public static PrivateKey readPrivateKey(String filename)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

}
