package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public class ClientCommands {
	private byte[] encryptedMsg;
	private PublicKey publicKey;
	private PrivateKey privateKey;
	private static final String PATIENTJOURNAL = "11";
	private static final String NURSEJOURNAL = "21";
	private static final String DOCTORJOURNAL = "31";

	public ClientCommands() {
	}

	public void sendMessage(PrintWriter out, String msg) {

		// if(msg.equalsIgnoreCase("1")){
		out.println(msg);
		out.flush();
		// }
	}

	public void recieveMessage(BufferedReader reader) {
		// String print;
		// try {
		// print = string.readLine().toString();
		// while (!(reader.isEmpty())) {
		// System.out.println(reader);
		// print = string.readLine().toString();
		// System.out.println("Printing");
		// } catch (IOException e1) {
		// TODO Auto-generated catch block
		// e1.printStackTrace();
	}

	// String flag = null;
	// String output;
	// try {
	// flag = in.readLine();
	// } catch (IOException e) {
	// return;
	// }
	// switch (flag) {
	//
	// case("Flag:readMessage"):
	// try {
	// while((output = in.readLine()) != null){
	// System.out.println(output);
	// }
	// } catch (IOException e) {
	// }
	//
	// case ("Flag:nurseChange"):
	// nurseChangeJournal();
	// }
	// }

	public void nurseChangeJournal() {
		// More functionality to be added
		System.out.println("What would you like to change?");
	}

	public byte[] encryptMessage(String msg) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException,
			UnsupportedEncodingException {
		return encryptedMsg = encrypt(publicKey, msg.getBytes("UTF8"));
	}

	public static byte[] encrypt(PublicKey key, byte[] plaintext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		return cipher.doFinal(plaintext);
	}

	public static byte[] decrypt(PrivateKey key, byte[] ciphertext)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");
		cipher.init(Cipher.DECRYPT_MODE, key);
		return cipher.doFinal(ciphertext);
	}

	public static byte[] readFileBytes(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	public static PublicKey readPublicKey(String filename) throws IOException,
			NoSuchAlgorithmException, InvalidKeySpecException {
		X509EncodedKeySpec publicSpec = new X509EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePublic(publicSpec);
	}

	public static PrivateKey readPrivateKey(String filename)
			throws IOException, NoSuchAlgorithmException,
			InvalidKeySpecException {
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(
				readFileBytes(filename));
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		return keyFactory.generatePrivate(keySpec);
	}

	public void handleMessage(String recoveredString) {
		switch (recoveredString) {
		case (PATIENTJOURNAL):
			System.out
			.println("These are your options: \n1: Get journal \nEnter your choice:");

		case (NURSEJOURNAL):

		case (DOCTORJOURNAL):

		}

	}
}
