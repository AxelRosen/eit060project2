#!/bin/bash

keytool -genkeypair -alias myServer -keystore serverkeystore

keytool -certreq -alias myServer -keystore serverkeystore -file myClient.csr

openssl x509 -req -days 365 -in myClient.csr -CA certificate.crt -CAkey privateKey.key -CAcreateserial -out client.crt

keytool -importcert -alias root -file certificate.crt -keystore serverkeystore

keytool -import -trustcacerts -alias myServer -file client.crt -keystore serverkeystore

keytool -import -file certificate.crt -alias myCA -keystore servertruststore